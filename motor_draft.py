# The file you call to start everything

from pyb import Pin, Timer
from machine import ADC
from br_serial import *
import br_timer
import time

#Pin definitions
pD5 = Pin('D5', Pin.OUT) #speed control motor 1
pD4 = Pin('D4', Pin.OUT) #direction
potmeter1 = ADC(Pin('A0', Pin.IN)) #potmeter
pD0 = Pin('D0', Pin.IN) #Encoder
pD1 = Pin('D1', Pin.IN) #Encoder

#setup serial connection
pc = serial_pc(1)

#initialize timers
timer1 = Timer(1, freq=1000)
timer4 = Timer(4, prescaler = 0, period = 0xFFFF)

#configure timers
pwmD5 = timer1.channel(2, Timer.PWM, pin=pD5)
timer4.channel(2, Timer.ENC_AB, pin = pD0)
timer4.channel(1, Timer.ENC_AB, pin = pD1)

def position_check(current_count, desired_count):
    speed = 30
    if (desired_count == current_count):
        print(desired_count, current_count, "desired_count = current_count")
    elif (desired_count < current_count):
        pD4.value(0) #set direction to cw of motor
        pwmD5.pulse_width_percent(speed)
        print(desired_count, current_count, "desired_count < current_count")
    else:
        pD4.value(1) #set direction to ccw of motor
        pwmD5.pulse_width_percent(speed)
        print(desired_count, current_count, "desired_count > current_count")
    return

def degree_to_count(degrees):
    #ration between count with respect to degree
    ration = 64/360    
    #count needed for desired degree in the output shaft
    count = 131.25 * ration * degrees
    return count

def run(degrees):
    desired_count = degree_to_count(degrees)
    current_count = timer4.counter()
    position_check(current_count, desired_count)
    return

while True:
    run(45)
    # pD4.value(1)
    # potvalue = potmeter1.read_u16()
    # percentage = 100 * potvalue / (2**16 - 1)
    # count = timer4.counter()
    # #print(count, percentage)
    # pwmD5.pulse_width_percent(percentage)
    time.sleep(0.01)

# def toggle_direction():
#     pD4.value() = not pD4.value()
#     return

# 131.25 revolutions for one shaft revolution
# thus
#   360 degrees is 131.25 * 64 counts
#   180 degrees is 131.25 * 64 * 0.5 counts
#   90 degrees is 131.25 * 64 * 0.25 counts
# 64 counts in encoder

# encoder_count = 0
# desired_count = degree_to_count(degrees)
# past_count = 0

# def current_count():
#     current_count = timer4.counter()
#     update_encoder_count(current_count)
#     count = count + encoder_count * 64
#     return count

# def update_encoder_count(current_count):
#     if pD4.value() == 1:
#         encoder_count += 1
#     elif pD4.value() == 0:
#         encoder_count -= 1
#     #calculate total count
#     past_count = current_count
#     return



