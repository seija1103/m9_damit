# Motor class that contains functionality to drive the motor with PWM. 
# You can create an instance of this class in the __init__ of state_functions.py, 
# to make sure the state functions can access the motor functionality.

from pin_definitions import Pins
from pyb import Pin, Timer

class Motor(object):

    def __init__(self, freq, motor=1):

        timer = Timer(1, freq=freq) # Must use timer 1 for PWM

        if motor == 1:
            pin = Pin(Pins.MOTOR1_PWM, Pin.OUT)
            self.pwm_pin = timer.channel(2, Timer.PWM, pin=pin)
            self.direction_pin = Pin(Pins.MOTOR1_DIRECTION, Pin.OUT)
        else:
            pin = Pin(Pins.MOTOR2_PWM, Pin.OUT)
            self.pwm_pin = timer.channel(1, Timer.PWM, pin=pin)
            self.direction_pin = Pin(Pins.MOTOR2_DIRECTION, Pin.OUT)
        return

    def toggle(self):
        # toggle boolean value of direction_pin
        new_direction = not self.direction_pin.value()
        self.direction_pin.value(new_direction)
        return

    #wrapper function
    def pulse_width_percentage(self, percentage):
        self.pwm_pin.pulse_widt_percentage(percentage)
        return