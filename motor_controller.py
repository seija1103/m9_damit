class MotorController(object):

    def __init__(self):


        return

    def position_check(current_count, desired_count):
        speed = 30
        if (desired_count == current_count):
            print(desired_count, current_count, "desired_count = current_count")
        elif (desired_count < current_count):
            pD4.value(0) #set direction to cw of motor
            pwmD5.pulse_width_percent(speed)
            print(desired_count, current_count, "desired_count < current_count")
        else:
            pD4.value(1) #set direction to ccw of motor
            pwmD5.pulse_width_percent(speed)
            print(desired_count, current_count, "desired_count > current_count")
        return

    def degree_to_count(degrees):
    #ration between count with respect to degree
    ration = 64/360    
    #count needed for desired degree in the output shaft
    count = 131.25 * ration * degrees
    return count

    def run(degrees):
        desired_count = degree_to_count(degrees)
        current_count = timer4.counter()
        position_check(current_count, desired_count)
        return