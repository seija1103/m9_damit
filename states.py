#Simple object that holds all the state names

class States(object):
    START = 'start'
    STOP = 'stop'
    RUN = 'run'
    READ = 'read'
    CONTROL = 'control'
    GRAB = 'grab'
    HOME = 'home'