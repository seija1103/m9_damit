# StateObject class that holds the current and past state has some 
# functionality to check whether the past state is the same as the 
# current state, and has some functionality to update the (past) state.

from states import States

class StateObject(object):

    def __init__(self):
        
        self.last_state = None
        self.state = States.START
        return

    def set_state(self, state):
        # Also changes last_state
        if self.last_state != self.state:
            self.last_state = self.state
        return