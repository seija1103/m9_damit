from pin_definitions import Pins
from pyb import Pin, Timer

class Encoder(object):

    def __init__(self, freq, encoder=1):

        if encoder == 1:
            #declare encoder 1 pins
            encA = Pin(Pins.ENCODER1_A, Pin.IN)
            encB = Pin(Pins.ENCODER1_B, Pin.IN)
            #assign timer 3 to encoder 1
            self.timer = Timer(3, prescaler = 0, period = 0xFFFF)
        else:
            #declare encoder 2 pins
            encA = Pin(Pins.ENCODER2_A, Pin.IN)
            encB = Pin(Pins.ENCODER2_B, Pin.IN)
            #assign timer 4 to encoder 2
            self.timer = Timer(4, prescaler = 0, period = 0xFFFF)

        #set timer channels to pins
        self.timer.channel(2, Timer.ENC_AB, pin = encA)
        self.timer.channel(1, Timer.ENC_AB, pin = encB)
        return

        def read(self):
            #read current encoder count
            return self.counter()