# Robot class that defines the start, stop, run for your robot, 
# and holds the state machine dictionary to link a state name to  state function.
# Make sure that the run function gets periodically called by the ticker, 
# that the run function first checks the button, and then executes the state 
# function from the state_machine dictionary

from states import States
from state_object import StateObject

class DAMit(object):

    def __init__(self, ticker_number, main_frequency):

        self.ticker_number = ticker_number
        self.main_frequency = main_frequency
        self.state_object = StateObject()

        self.state_machine = {
            States.START: self.start,
            States.STOP: self.stop,
            States.RUN: self.run
        }
        
        return
        
    def run(self):

        # Entry action

        # Action

        # State guards (transitions)

        return

    def start(self):

        # Entry action

        # Action

        # State guards (transitions)

        return

    def stop(self):

        # Entry action

        # Action

        # State guards (transitions)

        return
