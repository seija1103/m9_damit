# StateFunctions class that holds the functions corresponding to the states (safe, read, on), 
# and uses the potmeter and motor where applicable. None of the functions (safe, read, on) 
# has inputs, except for "self". The __init__ takes an instance from the StateObject class, 
# as described above. This way, both NucleoButtonControl and StateFunctions can have access 
# to the same mutable StateObject instance.

import br_serial
from potmeter import Potmeter
from motor import Motor

class StateFunctions(object):

    def __init__(self, state_object, EMG_value, motor_freq):
        self.state_object = state_object
        self.serial_pc = br_serial.serial_pc(1)
        return

    def read_emg(self, EMG_value):

        # Entry action

        # Action

        # State guards (transitions)

        return

    def control(self):

        # Entry action

        # Action

        # State guards (transitions)

        return

    def grab(self):

        # Entry action

        # Action

        # State guards (transitions)

        return
    
    def home(self):

        # Entry action

        # Action

        # State guards (transitions)

        return