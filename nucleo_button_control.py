# NucleoButtonControl class that contains functionality to update the state in 
# state_object using the blue button on the NUCLEO. The class' __init__ takes 
# an instance form the StateObject class. as described above. This way, both 
# NucleoButtonControl and StateFunctions can have access to the same mutable StateObject.