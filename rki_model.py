#import ulab as np
#from ulab import linalg
import numpy as np
import math

# Algemene info:
# vector.shape = (row, colom, diepte)


#Input
q1 = 2/3*math.pi        # Huidige angle (in radialen) van joint 1 --> afgelezen met de encoder van motor 1. 
                # Deze waarde wordt continue geupdate wanneer de robot in beweging is (immers, q1 en q2 veranderen dan) 
                # De mate waarin deze "joint updates" plaatsvinden is afhankelijk van de gekozen waarde voor delta_t. 
q2 = -1/4*math.pi          # Huidige angle (in radialen) van joint 2 --> afgelezen met de encoder van motor 2.
L1 = 0.25       # Lengte van link 1 (in meter)
L2 = 0.225      # Lengte van link 2 (in meter)
sx = 0.026      # Positieverandering van setpoint in de x richting (in meter) --> variabele afhankelijk van EMG input combinatie
sy = 0.026          # Positieverandering van setpoint in de y richting (in meter) --> variabele afhankelijk van EMG input combinatie
k = 10          # Veerconstante --> variabele zelf kiezen (nu op 10 gezet, moeten we met trial and error in DL)
b = 1           # Wrijvingscoefficient --> variabele zelf kiezen
delta_t = 1     # Aantal seconden waarin de loop wordt herhaald --> variabele zelf kiezen (nu op 1 gezet, moeten we testen met trial and error in DL)

# Positie van joint 2 t.o.v. frame 0 --> afhankelijk van q1
oP_j2 = [[math.cos(q1)*L1 ],
        [math.sin(q1)*L1]]
vector1 = np.array(oP_j2)
#print(vector1)

# Positie van end-effector t.o.v. frame 0 --> afhankelijk van q1 en q2
oP_e = [[(math.cos(q1)*L1)+(math.cos(q1+q2)*L2)],
        [(math.sin(q1)*L1)+(math.sin(q1+q2)*L2)]]
vector_2 = np.array(oP_e)
oP_e_x = vector_2[0:1] # x-coordinaat van end-effector positie t.o.v. frame 0
oP_e_y = vector_2[1:2] # y-coordinaat van end-effector positie t.o.v. frame 0
#print(vector_2)

# Positie van Set point (afhankelijk van je EMG input)
# sx is je x-coordinaat van de set point positie
# sy is je y-coordinaat van de set point positie
direction = [[sx], [sy]]
vector_d = np.array(direction)
oP_s = vector_2 + vector_d # positie van setpoint t.o.v. frame 0
#print(oP_s)

# Force of setpoint expressed in origin
Fs = k*(oP_s - vector_2)
F_x = Fs[0:1] # X component van de kracht
F_y = Fs[1:2] # Y component van de kracht
#print(Fs)

# Resulterende diagonaal die link 1 en 2 samen maken --> afhankelijk van q1 en q2
alpha = math.pi + q2
heart = math.sqrt((L1)**2 + (L2)**2 -(2*L1*L2*math.cos(alpha))) # de arm tussen joint 1 en de end-efffector
beta = math.acos(L1/heart) # de hoek tussen link 1 (L1) en heart (in radians) = de hoek tussen heart en r_y
gamma = 0.5*math.pi - beta # de hoek tussen r_x en heart
r_x = math.cos(gamma)*heart
r_y = math.sin(gamma)*heart
r =  [[r_x], [r_y]]
vector_r = np.array(r)
#print(vector_r)

# De Jacobian en de transposed Jacobian met daarin de twists van beide joints
T_1 = np.array([[1], [0], [0]])
T_2 = np.array([[1], [math.sin(q1)], [-1*math.cos(q1)]])
J = np.append(T_1, T_2,axis=1)
J_T = J.transpose()
#print(J)
#print(J_T)

# De (transposed = verticale matrix) Wrench met daarin de waarde voor het Moment, Fx en Fy op joint 1 en 2.  
M = np.array([r_x*F_y - r_y*F_x])
M.shape = (1, 1)
W = np.array([M, F_x, F_y])
W.shape = (3, 1)

# De (transposed) torque op joint 1 en 2. De resulterende vector (2,1) is de matrix vermenigvuldiging van J_T en W (dot product).
torque = np.dot(J_T, W)
torque_1 = torque[0:1]
torque_2 = torque[1:2]
#print(torque)

# Velocity van joint 1 en 2. Deze zijn gelijk aan de waardes van de torque voor joint 1 en 2. 
q1_dot_setpoint = torque_1/b
q2_dot_setpoint = torque_2/b

# De resulterende joint angles voor joint 1 en 2 om het ingestelde setpoint (op basis van EMG input) te bereiken.
# De waarden voor deze joint angles zijn afhankelijk van de huidige end-effector positie, de snelheid van de joint en de snelheid van de control loop (delta_t).
# De waarden van de setpoints worden continue geupdate (mate waarin dit gebeurd is afhankelijk van de snelheid van de control loop).
# Doordat er beweging plaatsvindt veranderen ook steeds de waarden van de "huidige positie". 
q1_setpoint = (beta+gamma) + (delta_t*q1_dot_setpoint) # Joint angle 1 in radians 
print("q1_setpoint", q1_setpoint)
q1_setpoint_degrees = math.degrees(q1_setpoint)
print("q1_setpoint_degrees", q1_setpoint_degrees)
q2_setpoint = (alpha-math.pi) + (delta_t*q2_dot_setpoint) # Joint angle 2 in radians 
print("q2_setpoint", q2_setpoint)
q2_setpoint_degrees = math.degrees(q2_setpoint)
print("q2_setpoint_degrees", q2_setpoint_degrees)
