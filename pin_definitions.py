# Simple object that holds all pin names (e.g. POTMETER1 = 'D13'). 
# Having this in a single object allows you to easily swap pins in a single place in the code

class Pins(object):
    NUCLEO_BLUE_BUTTON = 'C13'
    POTMETER1 = 'A5'
    POTMETER2 = ''
    BR_BUTTON1 = ''
    BR_BUTTON2 = ''
    BR_ENC1 = ''
    BR_ENC2 = ''
    MOTOR1_DIRECTION = 'D4'
    MOTOR1_PWM = 'D5'
    MOTOR2_PWM = 'D6'
    MOTOR2_DIRECTION = 'D7'
    ENCODER1_A = 'D0'
    ENCODER1_B = 'D1'
    ENCODER2_A = 'D11'
    ENCODER2_B = 'D12'
    EMG1 = 'A0'
    EMG2 = 'A1'
    EMG3 = 'A2'
    EMG4 = 'A3'
    END_EFFECTOR = ''



