# Potmeter class that contains functionality to read and return the potmeter 
# value using ADC. You can create an instance of this class in the __init__ 
# of state_functions.py, to make sure the state functions can access the 
# potmeter functionality.

from pin_definitions import Pins
from machine import ADC, Pin

class Potmeter(object):

    def __init__(self, potmeter=1):

        if potmeter == 1:
            self.pin = Pin(Pins.POTMETER1, Pin.IN)
            self.adc = ADC(self.pin)
        else:
            self.pin = Pin(Pins.POTMETER2, Pin.IN)
            self.adc = ADC(self.pin)
        return

    def read(self):
        # read analog signal in 16bit
        return self.adc.read_u16()
