import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
from scipy import signal

# simulate EMG signal
burst1 = np.random.uniform(-1, 1, size=1000) + 0.08
burst2 = np.random.uniform(-1, 1, size=1000) + 0.08
quiet = np.random.uniform(-0.05, 0.05, size=500) + 0.08
emg = np.concatenate([quiet, burst1, quiet, burst2, quiet])
time = np.array([i/1000 for i in range(0, len(emg), 1)]) # sampling rate 1000 Hz


# EMG max fixen en zorgen dat script t opslaat (MVC)


# EMG filters
def filteremg(time, emg, emg_max=1, low_pass=10, fs=1000, high_pass=20):
    """
    time: Time data
    emg: EMG data
    high: high-pass cut off frequency
    low: low-pass cut off frequency
    fs: sampling frequency
    """
    # Normalised EMG to EMGmax
    normalised = emg/emg_max

    # # normalise cut-off frequencies to sampling frequency
    # high_band = high_band/(fs/2)
    # low_band = low_band/(fs/2)
    
    # create bandpass filter for EMG
    # b1, a1 = sp.signal.butter(4, [high_band,low_band], btype='bandpass')
    
    # # create highpass filter for EMG
    high_pass = high_pass/(fs/2)
    b1, a1 = sp.signal.butter(4, high_pass, btype='highpass')
    emg_filtered = sp.signal.filtfilt(b1, a1, normalised)    
    
    # process EMG signal: rectify
    emg_rectified = abs(emg_filtered)
    
    # create lowpass filter and apply to rectified signal to get EMG envelope
    low_pass = low_pass/(fs/2)
    b2, a2 = sp.signal.butter(4, low_pass, btype='lowpass')
    emg_envelope = sp.signal.filtfilt(b2, a2, emg_rectified)
    
    # plot graphs
    # first graph : unrectified EMG
    fig = plt.figure()
    plt.subplot(1, 3, 1)
    plt.subplot(1, 3, 1).set_title('Unfiltered,' + '\n' + 'unrectified EMG')
    plt.plot(time, emg)
    plt.locator_params(axis='x', nbins=4)
    plt.locator_params(axis='y', nbins=4)
    plt.ylim(-1.5, 1.5)
    plt.xlabel('Time (sec)')
    plt.ylabel('EMG (Volt)')
    # second graph : rectified EMG    
    plt.subplot(1, 3, 2)
    plt.subplot(1, 3, 2).set_title('Filtered,' + '\n' + 'rectified EMG: ' + str(int(high_pass*(fs/2))) + '-' + str(int(low_pass*fs)) + 'Hz')
    plt.plot(time, emg_rectified)
    plt.locator_params(axis='x', nbins=4)
    plt.locator_params(axis='y', nbins=4)
    plt.ylim(-1.5, 1.5)
    # plt.plot([0.9, 1.0], [1.0, 1.0], 'r-', lw=5)
    plt.xlabel('Time (sec)')
    # third graph : rectified, EMG envelope
    plt.subplot(1, 3, 3)
    plt.subplot(1, 3, 3).set_title('Filtered, rectified ' + '\n' + 'EMG envelope: ' + str(int(low_pass*(fs/2))) + ' Hz')
    plt.plot(time, emg_envelope)
    plt.locator_params(axis='x', nbins=4)
    plt.locator_params(axis='y', nbins=4)
    plt.ylim(-1.5, 1.5)
    # plt.plot([0.9, 1.0], [1.0, 1.0], 'r-', lw=5)
    plt.xlabel('Time (sec)')
    # # fourth graph : focussed region
    # plt.subplot(1, 4, 4)
    # plt.subplot(1, 4, 4).set_title('Focussed region')
    # plt.plot(time[int(0.9*1000):int(1.0*1000)], emg_envelope[int(0.9*1000):int(1.0*1000)])
    # plt.locator_params(axis='x', nbins=4)
    # plt.locator_params(axis='y', nbins=4)
    # plt.xlim(0.9, 1.0)
    # plt.ylim(-1.5, 1.5)
    # plt.xlabel('Time (sec)')

    fig_name = 'fig_' + str(int(low_pass*(fs/2))) + '.png'
    fig.set_size_inches(w=11,h=7)
    fig.savefig(fig_name)

  # Check if EMG signal >= Threshold
    th_emg = 0.3  #waarde van threshold voor EMG 1(LA), baseren kalibratie EMG
    # th_emg2 = 0.3* emg_max2 #waarde van threshold voor EMG 2(RA), baseren kalibratie EMG
    # th_emg3 = 0.3 * emg_max3 #waarde van threshold voor EMG 3(LL), baseren kalibratie EMG
    EMG_1 = emg_envelope >= th_emg

    for element in EMG_1:
       print(element)
    



    # if EMG_2 >= th_emg2:
    #     EMG_2 is 1
    # else:
    #     EMG_2 is 0

    # if EMG_3 >= th_emg3:
    #     EMG_3 is True
    # else:
    #     EMG_3 is False

    # if EMG_1 is True:
    #     if EMG_2 is True:
    #         if EMG_3 is True:
    #             sx = -0.026
    #             sy = 0
    #         else:
    #             sx = 0.026
    #             sy = 0
    #     else:
    #         if EMG_3 is True:
    #             sx = -0.026
    #             sy = -0.026
    #         else:
    #             sx = 0.026
    #             sy = 0.026
    # else:
    #     if EMG_2 is True:
    #         if EMG_3 is True:
    #             sx = 0.026
    #             sy = -0.026
    #         else:
    #             sx = -0.026
    #             sy = 0.026

filteremg(time, emg)


# # show what different low pass filter cut-offs do
# for i in [3, 10, 40]:
#     filteremg(time, emg, low_pass=i)